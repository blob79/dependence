use std::collections::HashMap;
use std::hash::Hash;
use std::fmt::Debug;
use std::sync::mpsc::{Sender, Receiver};
use std::sync::mpsc;
use std::thread;


pub struct Dependence<T> {
    ingress : HashMap<T, i32>,
    deps : HashMap<T, Vec<T>>,
}

impl <T: Hash + Eq + Ord + Clone + Debug> Dependence<T> {

    pub fn new() -> Dependence<T> {
        Dependence{ ingress: HashMap::new(), deps: HashMap::new() }
    }

    pub fn len(&self) -> usize {
        self.ingress.len()
    }

    pub fn depends(&mut self, pre: T, suc: T) {
        assert!(!self.deps.get(&suc).unwrap_or(&vec![]).contains(&pre));

        self.ingress.entry(pre.clone()).or_insert(0);
        let si = self.ingress.entry(suc.clone()).or_insert(0);
        *si += 1;
        let mut sucs = self.deps.remove(&pre).unwrap_or(vec![]);
        sucs.push(suc);
        self.deps.insert(pre, sucs);
    }

    pub fn free(&mut self) -> Vec<T> {
        let mut vs : Vec<T>= self.deps
            .keys()
            .filter(|pre| {self.ingress(pre) == 0})
            .map(|pre| { pre.clone() } )
            .collect();
        vs.sort();
        vs
    }

    pub fn release(&mut self, pre: T) -> Vec<T> {
        let sucs = self.deps.remove(&pre).unwrap_or(vec![]);
        let mut free = vec![];
        for suc in sucs {
            let i = self.ingress(&suc);
            assert!(i > 0);
            if i > 1 {
              self.ingress.insert(suc, i - 1);
            } else {
              free.push(suc);
            }
        }
        free.sort();
        free
    }

    fn ingress(&self, t: &T) -> i32 {
        *(self.ingress.get(t).unwrap_or(&0))
    }
}

pub struct Done<T, R> {
    work: T, 
    result: R,
}

pub struct Work<T,R> {
    deps : Dependence<T>,
    fun : fn(T) -> R,
}

impl <T: Hash + Eq + Ord + Copy + Debug + Send + 'static, R : Eq + Send + 'static> Work<T, R> {
    pub fn new(f : fn(T) -> R ) -> Work<T, R> {
        Work{ deps: Dependence::new(), fun: f }
    }

    pub fn depends(&mut self, pre : T, suc: T) {
        self.deps.depends(pre, suc);  
    }

    pub fn work(&mut self) -> Vec<R> {
        let mut rs = vec![];
        let (tx, rx): (Sender<Done<T,R>>, Receiver<Done<T,R>>) = mpsc::channel();

        let n = self.deps.len();
        for work in self.deps.free() {
            let thread_tx = tx.clone();
            let f = self.fun;
            thread::spawn(move || {
                thread_tx.send(Done{work: work, result: f(work)}).unwrap();
            });
        }

        for _ in (0..n) {
            let done = rx.recv().unwrap();
            rs.push(done.result);
            for work in self.deps.release(done.work) {

              let thread_tx = tx.clone();
              let f = self.fun;
              thread::spawn(move || {
                thread_tx.send(Done{work: work, result: f(work)}).unwrap();
               });
            }
        }
        rs
    }
}

fn x(x: i32) -> i32 {
   x * 7
}

#[test]
fn work_empty() {
    let mut w = Work::new(x);
    assert!(w.work().is_empty());
}

#[test]
fn work_seq() {
    let mut w = Work::new(x);
    w.depends(1, 2);
    w.depends(2, 3);
    w.depends(3, 4);
    assert_eq!(vec![x(1), x(2), x(3), x(4)], w.work())
}

#[test]
fn work_tree() {
    let mut w = Work::new(x);
    w.depends(1, 2);
    w.depends(1, 3);
    let actual = w.work();
    assert!(vec![x(1), x(2), x(3)] == actual || vec![x(1), x(3), x(2)] == actual)
}

#[test]
fn seq() {
    let mut a = Dependence::new();
    a.depends(1, 2);
    assert_eq!(2, a.len());
    a.depends(2, 3);
    
    assert_eq!(3, a.len());
    assert_eq!(vec![1], a.free());
    assert_eq!(vec![2], a.release(1));
    assert!(a.free().is_empty());
    assert_eq!(vec![3], a.release(2));
    assert!(a.release(3).is_empty());
}

#[test]
fn tree() {
    let mut a = Dependence::new();
    a.depends(1, 2);
    a.depends(1, 3);
    
    assert_eq!(3, a.len());
    assert_eq!(vec![1], a.free());
    assert_eq!(vec![2, 3], a.release(1));
}

#[test]
fn inv_tree() {
    let mut a = Dependence::new();
    a.depends(1, 3);
    a.depends(2, 3);
    
    assert_eq!(3, a.len());
    assert_eq!(vec![1, 2], a.free());
    assert!(a.release(1).is_empty());
    assert_eq!(vec![3], a.release(2));
}

#[test]
fn release_twice() {
    let mut a = Dependence::new();
    a.depends(1, 2);
    assert_eq!(vec![2], a.release(1));
    assert!(a.release(1).is_empty());
}
 
#[test]
#[should_panic]
fn cycle() {
    let mut a = Dependence::new();
    a.depends(1, 2);
    a.depends(2, 1);
}
